/*************************************************************************
*
* Gbgoffice
* Copyright (C) 2004 Miroslav Yordanov <mironcho@linux-bg.org>
*
* This program is free software; you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation; either version 2 of the License, or
* (at your option) any later version.
*
* This program is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with this program; if not, write to the Free Software
* Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*
*************************************************************************/

#if !defined(ENABLE_LIGHT_VERSION) && !defined(DISABLE_TRAY)

#include "trayicon.h"
#include "dictgui.h"
#include "defaults.h"
#include <iostream>


TrayIcon::TrayIcon(DictGui *win)
:	mainwin(win)
{
	tray = egg_tray_icon_new("gbgoffice");
	icon = new Gtk::Image(std::string(FILES_DIR) + "gbgoffice-tray.png");
	
	box = new Gtk::EventBox;
	box->add(*icon);
	box->set_events(Gdk::BUTTON_PRESS_MASK);
	box->signal_button_press_event().connect(sigc::mem_fun(*this, &TrayIcon::on_button_press));

	gtk_container_add(GTK_CONTAINER(tray), GTK_WIDGET(box->gobj()));
	gtk_widget_show_all(GTK_WIDGET(tray));
	
	// pop up menu
	{
		Gtk::Menu::MenuList& menulist = popupmenu.items();
		menulist.push_back(Gtk::Menu_Helpers::StockMenuElem(Gtk::Stock::PREFERENCES, 
			         	sigc::mem_fun(*this, &TrayIcon::on_menu_preferences)));
		menulist.push_back(Gtk::Menu_Helpers::StockMenuElem(Gtk::Stock::HELP, 
			         	sigc::mem_fun(*this, &TrayIcon::on_menu_help)));
		menulist.push_back(Gtk::Menu_Helpers::StockMenuElem(Gtk::Stock::DIALOG_INFO,
			         	sigc::mem_fun(*this, &TrayIcon::on_menu_info)));
		
		menulist.push_back(Gtk::Menu_Helpers::SeparatorElem());
		menulist.push_back(Gtk::Menu_Helpers::StockMenuElem(Gtk::Stock::QUIT,
				sigc::mem_fun(*this, &TrayIcon::on_menu_quit)));
	}
}

TrayIcon::~TrayIcon()
{	
}

Gtk::Window *TrayIcon::getWindow()
{
	return Glib::wrap(GTK_WINDOW(tray));
}

bool TrayIcon::on_button_press(GdkEventButton* event)
{
	switch (event->button) {
		case 1:
			if (mainwin->win.is_visible()) {
				mainwin->win.hide();
			} else {
				mainwin->win.show();
			}
			break;
		case 3:
			popupmenu.popup(event->button, event->time);
			break;
		default:
			break;
	}
	return true;
}


void TrayIcon::on_menu_quit()
{
	this->getWindow()->hide();
}


void TrayIcon::on_menu_preferences()
{
	mainwin->on_menu_preferences();
}


void TrayIcon::on_menu_help()
{
	// show main window before put help into it
	mainwin->win.show();
	mainwin->on_menu_help();
}


void TrayIcon::on_menu_info()
{
	mainwin->on_menu_info();
}

#endif
